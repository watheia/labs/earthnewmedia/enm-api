/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */

import { Component, Binding } from "@loopback/core"

import { AuthController } from "./controllers/auth.controller"
import { BcryptHasher } from "./services/hash.password.bcryptjs"
import { JWTService } from "./services/jwt-service"
import { PasswordHasherBindings, TokenServiceBindings, TokenServiceConstants, UserServiceBindings } from "../keys"

export class AuthComponent implements Component {
  bindings = [
    // Bind JWT token service
    Binding.bind(TokenServiceBindings.TOKEN_SECRET).to(TokenServiceConstants.TOKEN_SECRET_VALUE),
    Binding.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE),
    Binding.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService),
    // Bind bcrypt hash service
    Binding.bind(PasswordHasherBindings.ROUNDS).to(10),
    Binding.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher),
  ]
  classes = {}
  controllers = [AuthController]
  lifeCycleObservers = []
  providers = {}
  servers = {}
}
