/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */

import { post, requestBody } from "@loopback/rest"
import { User } from "../../models"
import { inject } from "@loopback/core"
import { TokenService, UserService } from "@loopback/authentication"
import { CredentialsRequestBody } from "./specs/auth-controller.specs"
import { Credentials } from "../../repositories/user.repository"

import { TokenServiceBindings, UserServiceBindings } from "../../keys"
import _ from "lodash"
import { LoginSpec } from "./specs/auth-controller.specs"

import { LogLevel, log } from "../../log"
import { securityId } from "@loopback/security"

export class AuthController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>
  ) {}

  @log(LogLevel.INFO)
  @post("/login", LoginSpec)
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<{ token: string, userId: string }> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials)

    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user)

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile)
    const userId = userProfile[securityId]

    return { token, userId }
  }
}
