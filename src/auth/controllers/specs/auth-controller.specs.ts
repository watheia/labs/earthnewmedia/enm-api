/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */

const CredentialsSchema = {
  type: "object",
  required: ["email", "password"],
  properties: {
    email: {
      type: "string",
      format: "email",
    },
    password: {
      type: "string",
      minLength: 8,
    },
  },
}

export const CredentialsRequestBody = {
  description: "The input of login function",
  required: true,
  content: {
    "application/json": { schema: CredentialsSchema },
  },
}

export const LoginSpec = {
  responses: {
    "200": {
      description: "Token",
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              token: {
                type: "string",
              },
              userId: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
}
