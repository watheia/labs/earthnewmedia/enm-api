/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
export class IdStr extends String {}
export class UriStr extends String {}
export class NameStr extends String {}
export class EmailStr extends String {}
export class PhoneNumStr extends String {}
export class ZipCodeStr extends String {}

export enum AccessLevel {
  Private = "Private",
  Public = "Public",
}

export enum UserRole {
  Admin = "admin",
  Support = "support",
  Member = "member",
}

export enum LibraryType {
  Discussion = "Discussion",
  Album = "Album",
  Gallery = "Gallery",
}

export const ALL_USER_ROLES = [UserRole.Admin, UserRole.Support, UserRole.Member]
