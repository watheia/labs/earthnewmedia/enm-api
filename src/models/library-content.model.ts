/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { Entity, model, property, belongsTo } from "@loopback/repository"
import { Library } from "./library.model"

// TODO - well defined content types
@model({ settings: { strict: false } })
export class ContentMetadata {
  [prop: string]: any
}

@model()
export class LibraryContent extends Entity {
  @property({ id: true })
  id: string

  @property({ index: true })
  slug: string

  @belongsTo(() => Library)
  libraryId: string

  @property({
    required: true,
    jsonSchema: {
      minLength: 4,
      maxLength: 128,
    },
  })
  title: string

  @property()
  description?: string

  @property()
  createdDate: Date

  @property()
  lastUpdate: Date

  @property({ required: true, type: "object" })
  metadata: ContentMetadata

  constructor(data?: Partial<LibraryContent>) {
    super(data)
  }
}
