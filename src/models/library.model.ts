/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { Entity, model, property, belongsTo, hasMany } from "@loopback/repository"
import { User } from "./user.model"
import { LibraryContent } from "./library-content.model"
import { AccessLevel, LibraryType } from "./base.model"

@model()
export class Library extends Entity {
  @property({ id: true })
  id: string

  @property({ index: true })
  slug: string

  @belongsTo(() => User)
  userId: string

  @property({ required: true, type: "string" })
  access: AccessLevel

  @property({ required: true, type: "string" })
  libraryType: LibraryType

  @property({
    required: true,
    jsonSchema: {
      minLength: 4,
      maxLength: 64,
    },
  })
  name: string

  @property()
  description: string

  @property()
  createdDate: Date

  @property()
  lastUpdate: Date

  @hasMany(() => LibraryContent)
  content: LibraryContent[]

  constructor(data?: Partial<Library>) {
    super(data)
  }
}
