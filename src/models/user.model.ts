import { Entity, model, property, hasMany, hasOne } from "@loopback/repository"
import { UserCredentials } from "./user-credentials.model"
import { Library } from "./library.model"
import { UserRole } from "./base.model"

const UserSpec = {
  settings: {
    hiddenProperties: ["userCredentials"],
    indexes: {
      uniqueEmail: {
        keys: { email: 1 },
        options: { unique: true }
      }
    }
  }
}

@model(UserSpec)
export class User extends Entity {
  @property({ id: true })
  id: string

  @property({
    jsonSchema: {
      format: "email",
      transform: ["toLowerCase"]
    }
  })
  email: string

  @property({
    jsonSchema: {
      minLength: 4,
      maxLength: 32
    }
  })
  userName: string

  @property({
    index: true,
    jsonSchema: {
      minLength: 4,
      maxLength: 32
    }
  })
  slug: string

  @property.array(String)
  roles: UserRole[]

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials

  @hasMany(() => Library)
  libraries: Library[]

  hasRole(role: UserRole): boolean {
    return this.roles.includes(role)
  }

  constructor(data?: Partial<User>) {
    super(data)
  }
}
