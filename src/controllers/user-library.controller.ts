/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */

import { repository, Where, Count } from "@loopback/repository"
import { UserRepository, LibraryRepository } from "../repositories"
import { post, patch, del, param, requestBody, HttpErrors, getModelSchemaRef } from "@loopback/rest"
import { Library, ALL_USER_ROLES } from "../models"
import { authorize } from "@loopback/authorization"
import { authenticate } from "@loopback/authentication"
import { basicAuthorization } from "../services/basic.authorizer"
import { CreateLibrarySpec, UpdateLibrarySpec, DeleteLibrarySpec } from "./specs/user-library-controller.spec"
import { UserProfile, SecurityBindings, securityId } from "@loopback/security"
import { inject } from "@loopback/core"

/**
 * Controller for User Libraries
 */
export class UserLibraryController {
  constructor(
    @repository(LibraryRepository)
    public libraryRepo: LibraryRepository,
    @repository(UserRepository) protected userRepo: UserRepository
  ) {}

  /**
   * Create or update the libraries for a given user
   * @param userId User id
   * @param cart Shopping cart
   */
  @post("/users/{userId}/libraries", CreateLibrarySpec)
  @authenticate("jwt")
  @authorize({ allowedRoles: ALL_USER_ROLES, voters: [basicAuthorization] })
  async create(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("userId")
    userId: string,
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(Library, {
            title: "NewLibraryContent",
            exclude: ["id"],
          }),
        },
      },
    })
    library: Omit<Library, "id">
  ): Promise<Library> {
    // TODO allow action for Admin
    if (userId !== principal[securityId]) {
      throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
    }

    return this.userRepo
      .libraries(userId)
      .create(library)
      .catch((e) => {
        throw HttpErrors(400)
      })
  }

  @patch("/users/{userId}/libraries", UpdateLibrarySpec)
  @authenticate("jwt")
  @authorize({ allowedRoles: ALL_USER_ROLES, voters: [basicAuthorization] })
  async update(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("userId")
    userId: string,
    @requestBody()
    library: Library
  ): Promise<Library> {
    // TODO allow action for Admin
    if (userId !== principal[securityId]) {
      throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
    }

    await this.libraryRepo.update(library)
    return library
  }

  @del("/users/{userId}/libraries", DeleteLibrarySpec)
  @authenticate("jwt")
  @authorize({ allowedRoles: ALL_USER_ROLES, voters: [basicAuthorization] })
  async delete(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("userId")
    userId: string,
    @param.query.string("where")
    where?: Where<Library>
  ): Promise<Count> {
    // TODO allow action for Admin
    if (userId !== principal[securityId]) {
      throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
    }

    return this.userRepo.libraries(userId).delete(where)
  }
}
