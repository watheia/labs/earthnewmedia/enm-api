// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { repository, model, property, Filter } from "@loopback/repository"
import { validateCredentials } from "../services/validator"
import { post, put, param, get, requestBody, HttpErrors, getModelSchemaRef } from "@loopback/rest"
import { User, UserRole } from "../models"
import { UserRepository } from "../repositories"
import { inject } from "@loopback/core"
import { authenticate, TokenService, UserService } from "@loopback/authentication"
import { authorize } from "@loopback/authorization"
import { UserProfile, securityId, SecurityBindings } from "@loopback/security"
import { CreateUserSpec, UpdateUserSpec, GetUserSpec, GetSelfSpec } from "./specs/user-controller.specs"
import { Credentials } from "../repositories/user.repository"
import { PasswordHasher } from "../auth/services/hash.password.bcryptjs"

import { TokenServiceBindings, PasswordHasherBindings, UserServiceBindings } from "../keys"
import { omit, pick } from "lodash"
import { basicAuthorization } from "../services/basic.authorizer"

import { LogLevel, log } from "../log"

@model()
export class NewUserRequest {
  @property({ required: true })
  email: string
  @property({ required: true })
  userName: string
  @property({ required: true })
  password: string
}

export class UserController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: UserService<User, Credentials>
  ) {}

  @log(LogLevel.INFO)
  @post("/users", CreateUserSpec)
  async create(
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(NewUserRequest, {
            title: "NewUser"
          })
        }
      }
    })
    payload: NewUserRequest
  ): Promise<{ token: string }> {
    // All new users have the UserRole.MEMBER role by default
    // ensure a valid email value and password value
    validateCredentials(pick(payload, ["email", "password"]))

    // encrypt the password
    const password = await this.passwordHasher.hashPassword(payload.password)

    try {
      // create the new user
      const savedUser = await this.userRepository.create({
        ...omit(payload, "password"),
        roles: [UserRole.Member]
      })

      // set the password
      await this.userRepository.userCredentials(savedUser.id).create({ password })
      const userProfile = this.userService.convertToUserProfile(savedUser)
      const token = await this.jwtService.generateToken(userProfile)
      return { token }
    } catch (error) {
      // MongoError 11000 duplicate key
      // TODO validate userName
      if (error.code === 11000 && error.errmsg.includes("index: uniqueEmail")) {
        throw new HttpErrors.Conflict("Email is already in use")
      } else {
        throw error
      }
    }
  }

  @log(LogLevel.INFO)
  @put("/users/{userId}", UpdateUserSpec)
  @authenticate("jwt")
  @authorize({
    allowedRoles: [UserRole.Admin, UserRole.Support],
    voters: [basicAuthorization]
  })
  async update(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("userId") userId: string,
    @requestBody({ description: "update user" }) user: User
  ): Promise<User> {
    try {
      if (!principal.roles.includes(UserRole.Admin)) {
        if (principal[securityId] !== userId) {
          throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
        }

        // Only admin can assign roles
        delete user.roles
      }
      await this.userRepository.updateById(userId, user)
      return await this.userRepository.findById(userId)
    } catch (e) {
      return e
    }
  }

  @log(LogLevel.INFO)
  @get("/users/{userSlug}", GetUserSpec)
  @authenticate("jwt")
  async get(
    @param.path.string("userSlug")
    userSlug: string,
    @param.query.object("filter")
    filter?: Filter<User>
  ): Promise<User> {
    const user = await this.userRepository.findBySlug(userSlug, filter)
    if (user === null) {
      throw new HttpErrors.NotFound("User not found")
    }

    return user
  }

  @log(LogLevel.INFO)
  @get("/users/me", GetSelfSpec)
  @authenticate("jwt")
  async printCurrentUser(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.query.object("filter")
    filter?: Filter<User>
  ): Promise<User> {
    filter = filter ? filter : { include: [{ relation: "libraries" }] }
    const userId = principal[securityId]
    return this.userRepository.findById(userId, filter)
  }
}
