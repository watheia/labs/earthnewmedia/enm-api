/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { Count, repository, Where } from "@loopback/repository"
import { post, param, getModelSchemaRef, requestBody, HttpErrors, patch, del } from "@loopback/rest"
import { LibraryContent, ALL_USER_ROLES } from "../models"
import { LibraryContentRepository, LibraryRepository } from "../repositories"
import {
  CreateLibraryContentSpec,
  UpdateLibraryContentSpec,
  DeleteLibraryContentSpec,
} from "./specs/library-content-controller.specs"
import { authenticate } from "@loopback/authentication"
import { authorize } from "@loopback/authorization"
import { basicAuthorization } from "../services/basic.authorizer"
import { inject } from "@loopback/core"
import { SecurityBindings, UserProfile, securityId } from "@loopback/security"

export class LibraryContentController {
  constructor(
    @repository(LibraryRepository)
    public libraryRepo: LibraryRepository,
    @repository(LibraryContentRepository)
    public contentRepo: LibraryContentRepository
  ) {}

  @post("/library-content/{libraryId}", CreateLibraryContentSpec)
  @authenticate("jwt")
  @authorize({ allowedRoles: ALL_USER_ROLES, voters: [basicAuthorization] })
  async create(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("libraryId")
    libraryId: string,
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(LibraryContent, {
            title: "New LibraryContent",
            exclude: ["id"],
          }),
        },
      },
    })
    payload: Omit<LibraryContent, "id">
  ): Promise<LibraryContent> {
    // Ensure library exists
    const library = await this.libraryRepo.findById(libraryId)
    if (!library) {
      throw HttpErrors(404)
    }

    // Only allow updates for entities owned by principal
    // TODO allow action for Admin
    if (library.userId !== principal[securityId]) {
      throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
    }

    return this.libraryRepo.content(libraryId).create(payload)
  }

  @patch("/library-content/{libraryId}", UpdateLibraryContentSpec)
  @authenticate("jwt")
  @authorize({ allowedRoles: ALL_USER_ROLES, voters: [basicAuthorization] })
  async update(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("libraryId")
    libraryId: string,
    @requestBody({
      content: {
        "application/json": {
          schema: getModelSchemaRef(LibraryContent),
        },
      },
    })
    payload: LibraryContent
  ): Promise<LibraryContent> {
    // Ensure library exists
    const library = await this.libraryRepo.findById(libraryId)
    if (!library) {
      throw HttpErrors(404)
    }

    // Only allow updates for entities owned by principal
    // TODO allow action for Admin
    if (library.userId !== principal[securityId]) {
      throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
    }

    await this.contentRepo.update(payload)
    return payload
  }

  @del("/library-content/{libraryId}", DeleteLibraryContentSpec)
  @authenticate("jwt")
  @authorize({ allowedRoles: ALL_USER_ROLES, voters: [basicAuthorization] })
  async delete(
    @inject(SecurityBindings.USER)
    principal: UserProfile,
    @param.path.string("libraryId")
    libraryId: string,
    @param.query.string("where")
    where?: Where<LibraryContent>
  ): Promise<Count> {
    // Ensure library exists
    const library = await this.libraryRepo.findById(libraryId)
    if (!library) {
      throw HttpErrors(404)
    }

    // Only allow updates for entities owned by principal
    // TODO allow action for Admin
    if (library.userId !== principal[securityId]) {
      throw new HttpErrors.Unauthorized("Principal unauthorized for this action.")
    }

    return this.libraryRepo.content(libraryId).delete(where)
  }
}
