/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { OPERATION_SECURITY_SPEC } from "../../utils/security-spec"
import { Library } from "../../models"
import { CountSchema } from "@loopback/repository"
import { getModelSchemaRef } from "@loopback/rest"

export const CreateLibrarySpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "User.Library model instance",
      content: { "application/json": { schema: getModelSchemaRef(Library) } },
    },
  },
}

export const UpdateLibrarySpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "User.Library model instance",
      content: { "application/json": { schema: getModelSchemaRef(Library) } },
    },
  },
}

export const DeleteLibrarySpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "User.Library DELETE success count",
      content: { "application/json": { schema: CountSchema } },
    },
  },
}
