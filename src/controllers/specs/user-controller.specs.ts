/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { User } from "../../models"

import { OPERATION_SECURITY_SPEC } from "../../utils/security-spec"
import { getModelSchemaRef } from "@loopback/rest"

// TODO(jannyHou): This should be moved to @loopback/authentication
export const UserProfileSchema = {
  type: "object",
  required: ["id"],
  properties: {
    id: { type: "string" },
    email: { type: "string" },
    name: { type: "string" },
  },
}

export const CreateUserSpec = {
  responses: {
    "200": {
      description: "JWT Token",
      content: {
        "application/json": {
          schema: {
            type: "object",
            properties: {
              token: {
                type: "string",
              },
            },
          },
        },
      },
    },
  },
}

export const UpdateUserSpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "User.Library model instance",
      content: { "application/json": { schema: getModelSchemaRef(User) } },
    },
  },
}

export const GetUserSpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "User",
      content: {
        "application/json": {
          schema: {
            "x-ts-type": User,
          },
        },
      },
    },
  },
}

export const GetSelfSpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "The current user profile",
      content: {
        "application/json": {
          schema: UserProfileSchema,
        },
      },
    },
  },
}
