/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { OPERATION_SECURITY_SPEC } from "../../utils/security-spec"
import { LibraryContent } from "../../models"
import { CountSchema } from "@loopback/repository"
import { getModelSchemaRef } from "@loopback/rest"

export const CreateLibraryContentSpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "LibraryContent model instance",
      content: { "application/json": { schema: getModelSchemaRef(LibraryContent) } },
    },
  },
}

export const UpdateLibraryContentSpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "LibraryContent model instance",
      content: { "application/json": { schema: getModelSchemaRef(LibraryContent) } },
    },
  },
}

export const DeleteLibraryContentSpec = {
  security: OPERATION_SECURITY_SPEC,
  responses: {
    "200": {
      description: "LibraryContent DELETE success count",
      content: { "application/json": { schema: CountSchema } },
    },
  },
}
