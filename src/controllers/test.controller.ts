/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */

import { model } from "@loopback/repository"
import { getModelSchemaRef, requestBody, post, param, get } from "@loopback/rest"
import { log, LogLevel } from "../log"
import { TestFixturesService } from "../services"
import { service } from "@loopback/core"

@model({ settings: { strict: false } })
class GreetingPayload {
  // Add an indexer property to allow additional data
  [prop: string]: any
}

@model()
class Greeting {
  message: string
  payload: GreetingPayload

  constructor(message: string, payload: GreetingPayload) {
    this.message = message
    this.payload = payload
  }
}

const GreetingSpec = {
  responses: {
    "200": {
      description: "greeting response",
      content: { "application/json": { schema: getModelSchemaRef(Greeting) } },
    },
  },
}

const InitTestFixturesSpec = {
  responses: {
    "200": {
      description: "Init test Fixtures Response",
      content: {
        "application/json": {
          schema: {
            type: "object",
            title: "InitFixturesResponse",
            properties: {
              date: { type: "string" },
              headers: {
                type: "object",
                properties: {
                  "Content-Type": { type: "string" },
                },
                additionalProperties: true,
              },
            },
          },
        },
      },
    },
  },
}

export const GreetingRequestBody = {
  description: "The greeting request",
  required: false,
  content: { "application/json": { schema: getModelSchemaRef(GreetingPayload) } },
}

export class TestController {
  testFixtures: TestFixturesService

  constructor(@service(TestFixturesService) testFixtures: TestFixturesService) {
    this.testFixtures = testFixtures
  }

  @log(LogLevel.INFO)
  @post("/test/greeting/{name}", GreetingSpec)
  async greeting(
    @param.path.string("name")
    name: string,
    @requestBody(GreetingRequestBody)
    payload: GreetingPayload
  ): Promise<Greeting> {
    return new Greeting(`Hello, ${name}!`, payload)
  }

  @get("/test/init-fixtures", InitTestFixturesSpec)
  async init(): Promise<{ date: Date }> {
    await this.testFixtures.init()
    return { date: new Date() }
  }
}
