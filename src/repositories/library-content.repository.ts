/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { DefaultCrudRepository, juggler, DataObject, Options, Filter } from "@loopback/repository"
import { LibraryContent } from "../models"
import { inject } from "@loopback/core"
import { toSlug } from "../utils/slug"

export class LibraryContentRepository extends DefaultCrudRepository<
  LibraryContent,
  typeof LibraryContent.prototype.id
> {
  constructor(@inject("datasources.mongo") protected datasource: juggler.DataSource) {
    super(LibraryContent, datasource)
  }

  async findBySlug(slug: string, filter?: Filter<LibraryContent>): Promise<LibraryContent | null> {
    filter = filter ? filter : {}
    filter.where = filter.where ? { ...filter.where, slug } : { slug }
    return this.findOne(filter)
  }

  async create(entity: DataObject<LibraryContent>, options?: Options): Promise<LibraryContent> {
    if (!entity.title) throw new Error("Attempting to create LibraryContent without a title")

    entity.slug = toSlug(entity.title)
    entity.createdDate = entity.lastUpdate = new Date()
    return super.create(entity, options)
  }

  async update(entity: LibraryContent, options?: Options): Promise<void> {
    if (!entity.title) throw new Error("Attempting to update LibraryContent without a title")

    entity.slug = toSlug(entity.title)
    entity.lastUpdate = new Date()
    return super.update(entity, options)
  }
}
