// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {
  DefaultCrudRepository,
  juggler,
  HasManyRepositoryFactory,
  repository,
  HasOneRepositoryFactory,
  DataObject,
  Options,
  Filter
} from "@loopback/repository"
import { User, UserCredentials, Library } from "../models"
import { inject, Getter } from "@loopback/core"
import { UserCredentialsRepository } from "./user-credentials.repository"
import { LibraryRepository } from "./library.repository"
import { toSlug } from "../utils/slug"

export type Credentials = {
  email: string
  password: string
}

export class UserRepository extends DefaultCrudRepository<User, typeof User.prototype.id> {
  public readonly userCredentials: HasOneRepositoryFactory<UserCredentials, typeof User.prototype.id>

  public libraries: HasManyRepositoryFactory<Library, typeof User.prototype.id>

  constructor(
    @inject("datasources.mongo")
    protected datasource: juggler.DataSource,
    @repository.getter("LibraryRepository")
    protected libraryRepositoryGetter: Getter<LibraryRepository>,
    @repository.getter("UserCredentialsRepository")
    protected userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>
  ) {
    super(User, datasource)

    this.userCredentials = this.createHasOneRepositoryFactoryFor("userCredentials", userCredentialsRepositoryGetter)

    this.libraries = this.createHasManyRepositoryFactoryFor("libraries", libraryRepositoryGetter)
    this.registerInclusionResolver("libraries", this.libraries.inclusionResolver)
  }

  async findBySlug(slug: string, filter?: Filter<User>): Promise<User | null> {
    filter = filter ? filter : { include: [{ relation: "libraries" }] }
    filter.where = filter.where ? { ...filter.where, slug } : { slug }
    return this.findOne(filter)
  }

  async create(entity: DataObject<User>, options?: Options): Promise<User> {
    if (!entity.userName) throw new Error("Attempting to create user without userName")

    entity.slug = toSlug(entity.userName)
    return super.create(entity, options)
  }

  async update(entity: User, options?: Options): Promise<void> {
    if (!entity.userName) throw new Error("Attempting to update user without userName")

    entity.slug = toSlug(entity.userName)
    return super.update(entity, options)
  }

  async findCredentials(userId: typeof User.prototype.id): Promise<UserCredentials | undefined> {
    try {
      return await this.userCredentials(userId).get()
    } catch (err) {
      if (err.code === "ENTITY_NOT_FOUND") {
        return undefined
      }
      throw err
    }
  }
}
