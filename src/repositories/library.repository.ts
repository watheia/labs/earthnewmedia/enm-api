/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */

import { DefaultCrudRepository, juggler, repository, HasManyRepositoryFactory, DataObject, Options, Filter } from "@loopback/repository"
import { Library, LibraryContent } from "../models"
import { inject, Getter } from "@loopback/core"
import { LibraryContentRepository } from "./library-content.repository"
import { toSlug } from "../utils/slug"

export class LibraryRepository extends DefaultCrudRepository<Library, typeof Library.prototype.id> {
  public content: HasManyRepositoryFactory<LibraryContent, typeof Library.prototype.id>

  constructor(
    @inject("datasources.mongo")
    protected datasource: juggler.DataSource,
    @repository.getter("LibraryContentRepository")
    protected contentRepositoryGetter: Getter<LibraryContentRepository>
  ) {
    super(Library, datasource)
    this.content = this.createHasManyRepositoryFactoryFor("content", contentRepositoryGetter)
    this.registerInclusionResolver("content", this.content.inclusionResolver)
  }

  async findBySlug(slug: string, filter?: Filter<Library>): Promise<Library | null> {
    filter = filter ? filter : {}
    filter.where = filter.where ? { ...filter.where, slug } : { slug }
    return this.findOne(filter)
  }

  async create(entity: DataObject<Library>, options?: Options): Promise<Library> {
    if(!entity.name) throw  new Error("Attempting to create Library without a name")

    entity.slug = toSlug(entity.name)
    entity.createdDate = entity.lastUpdate = new Date()
    return super.create(entity, options)
  }

  async update(entity: Library, options?: Options): Promise<void> {
    if(!entity.name) throw new Error("Attempting to update Library without a name")

    entity.slug = toSlug(entity.name)
    entity.lastUpdate = new Date()
    return super.update(entity, options)
  }

}
