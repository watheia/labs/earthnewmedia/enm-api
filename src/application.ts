// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { BootMixin } from "@loopback/boot"
import { ApplicationConfig, BindingKey } from "@loopback/core"
import { RepositoryMixin, SchemaMigrationOptions } from "@loopback/repository"
import { RestApplication } from "@loopback/rest"
import { ServiceMixin } from "@loopback/service-proxy"
import { MyAuthenticationSequence } from "./sequence"
import { RestExplorerBindings, RestExplorerComponent } from "@loopback/rest-explorer"
import { PasswordHasherBindings, UserServiceBindings, TestFixtureServiceBindings } from "./keys"
import { MyUserService } from "./services/user-service"
import _ from "lodash"
import path from "path"
import { AuthenticationComponent, registerAuthenticationStrategy } from "@loopback/authentication"
import { JWTAuthenticationStrategy } from "./authentication-strategies/jwt-strategy"
import { SECURITY_SCHEME_SPEC } from "./utils/security-spec"
import { AuthorizationComponent } from "@loopback/authorization"

import { AuthComponent } from "./auth"
import { LogComponent, LogMixin, LogLevel } from "./log"
import { TestFixturesService } from "./services"
import { UserRepository, UserCredentialsRepository, LibraryContentRepository, LibraryRepository } from "./repositories"
import { SchemaGeneratorService } from "./services/schema-generator"

/**
 * Information from package.json
 */
export interface PackageInfo {
  name: string
  version: string
  description: string
}
export const PackageKey = BindingKey.create<PackageInfo>("application.package")

const pkg: PackageInfo = require("../package.json")

export class Application extends LogMixin(BootMixin(ServiceMixin(RepositoryMixin(RestApplication)))) {
  constructor(options?: ApplicationConfig) {
    super(options)

    /*
       This is a workaround until an extension point is introduced
       allowing extensions to contribute to the OpenAPI specification
       dynamically.
    */
    this.api({
      openapi: "3.0.0",
      info: { title: pkg.name, version: pkg.version },
      paths: {},
      components: { securitySchemes: SECURITY_SCHEME_SPEC },
      servers: [{ url: "/" }]
    })

    this.logLevel(LogLevel.INFO)

    this.setUpBindings()

    // Bind authentication component related elements
    this.component(AuthenticationComponent)
    this.component(AuthorizationComponent)

    // Auth implementation component
    this.component(AuthComponent)

    // Log impl component
    this.component(LogComponent)

    this.service(TestFixturesService)
    this.service(SchemaGeneratorService)

    // authentication
    registerAuthenticationStrategy(this, JWTAuthenticationStrategy)

    // Set up the custom sequence
    this.sequence(MyAuthenticationSequence)

    // Set up default home page
    this.static("/", path.join(__dirname, "../public"))

    // Customize @loopback/rest-explorer configuration here
    this.bind(RestExplorerBindings.CONFIG).to({
      path: "/explorer"
    })
    this.component(RestExplorerComponent)

    this.projectRoot = __dirname
    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ["controllers"],
        extensions: [".controller.js"],
        nested: true
      }
    }
  }

  setUpBindings(): void {
    // Bind package.json to the application context
    this.bind(PackageKey).to(pkg)
    this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService)
  }

  async start() {
    // Use `databaseSeeding` flag to control if products/users should be pre
    // populated into the database. Its value is default to `true`.
    if (this.options.databaseSeeding !== false) {
      await this.migrateSchema()
    }

    this.generateSchemas()
    return super.start()
  }

  generateSchemas(): void {
    const schemaGenerator = new SchemaGeneratorService()
    schemaGenerator.generate()
  }

  async migrateSchema(options?: SchemaMigrationOptions) {
    await super.migrateSchema(options)

    const passwordHasher = await this.get(PasswordHasherBindings.PASSWORD_HASHER)
    const userRepo = await this.getRepository(UserRepository)
    const credentialsRepo = await this.getRepository(UserCredentialsRepository)
    const libraryRepo = await this.getRepository(LibraryRepository)
    const libraryContentRepo = await this.getRepository(LibraryContentRepository)

    const testFixtures = new TestFixturesService(passwordHasher, userRepo, credentialsRepo, libraryRepo, libraryContentRepo)
    await testFixtures.init()
  }
}
