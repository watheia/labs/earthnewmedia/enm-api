/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import slugify from "slugify"

export function toSlug(content: string): string {
  return slugify(content, {
    lower: true,
    strict: true,
  })
}
