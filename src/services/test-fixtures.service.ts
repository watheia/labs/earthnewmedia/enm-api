/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 */
import { inject } from "@loopback/core"
import path from "path"
import fs from "fs"
import _ from "lodash"
import { repository, model, property } from "@loopback/repository"
import { UserRepository, UserCredentialsRepository, LibraryRepository, LibraryContentRepository } from "../repositories"
import { PasswordHasherBindings } from "../keys"
import { PasswordHasher } from "../auth"
import YAML = require("yaml")
import { User } from "../models"

@model()
export class NewUser extends User {
  @property({
    type: "string",
    required: true,
  })
  password: string
}

export class TestFixturesService {
  passwordHasher: PasswordHasher
  userRepo: UserRepository
  credentialsRepo: UserCredentialsRepository
  libraryRepo: LibraryRepository
  libraryContentRepo: LibraryContentRepository
  constructor(
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    passwordHasher: PasswordHasher,
    @repository(UserRepository)
    userRepo: UserRepository,
    @repository(UserCredentialsRepository)
    credentialsRepo: UserCredentialsRepository,
    @repository(LibraryRepository)
    libraryRepo: LibraryRepository,
    @repository(LibraryContentRepository)
    libraryContentRepo: LibraryContentRepository
  ) {
    this.userRepo = userRepo
    this.credentialsRepo = credentialsRepo
    this.libraryRepo = libraryRepo
    this.libraryContentRepo = libraryContentRepo
    this.passwordHasher = passwordHasher
  }

  public async init(): Promise<boolean> {
    // Clear existing data
    await this.userRepo.deleteAll()
    await this.credentialsRepo.deleteAll()
    await this.libraryRepo.deleteAll()
    await this.libraryContentRepo.deleteAll()

    // Create test users
    const usersDir = path.join(__dirname, "../../fixtures/users")
    const userFiles = fs.readdirSync(usersDir)
    for (const file of userFiles) {
      if (file.endsWith(".yml")) {
        const userFile = path.join(usersDir, file)
        const yamlString = YAML.parse(fs.readFileSync(userFile, "utf8"))
        const input = new NewUser(yamlString)
        const password = await this.passwordHasher.hashPassword(input.password)
        input.password = password
        const user = await this.userRepo.create(_.omit(input, "password"))

        await this.userRepo.userCredentials(user.id).create({ password })
      }
    }

    return true
  }
}
