import path from "path"
import fs from "fs"
import { User, UserCredentials, Library, LibraryContent } from "../models"
import { getJsonSchema } from "@loopback/repository-json-schema"

export class SchemaGeneratorService {
  outputDir: string

  constructor() {
    this.outputDir = path.join(__dirname, "../../schemas")
  }

  public generate(): void {
    this.writeSchema(getJsonSchema(User), "user.schema.json")
    this.writeSchema(getJsonSchema(UserCredentials), "user-credentials.schema.json")
    this.writeSchema(getJsonSchema(Library), "library.schema.json")
    this.writeSchema(getJsonSchema(LibraryContent), "library-content.schema.json")
  }

  protected writeSchema(schema: object, file: string): void {
    fs.writeFile(path.join(this.outputDir, file), JSON.stringify(schema, null, 2), err => {
      if (err) {
        console.error("(err) = ", err)
      }
    })
  }
}
