import { LibraryType } from "./models"

export const DEFAULT_LIBRARY = {
  name: "My Library",
  description: "This is your default library. You may attach content to this or any other library you create.",
  createdDate: new Date(),
  libraryType: LibraryType.Discussion
}

export const FIRST_POST = {
  title: "First Post",
  description: "This is your first post!",
  createdDate: new Date(),
  metadata: {
    uri: "http://localhost:3001/#/johnDoe",
    mimetype: "text/html",
    icon: "img/placeholder-72x72.jpg"
  }
}
