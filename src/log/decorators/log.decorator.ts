// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-log-extension
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { LogLevel, LogMetadataKey } from "../keys"
import { Constructor, MethodDecoratorFactory, MetadataInspector } from "@loopback/context"
import { LevelMetadata } from "../types"

/**
 * Mark a controller method as requiring logging (input, output & timing)
 * if it is set at or greater than Application LogLevel.
 * LogLevel.DEBUG < LogLevel.INFO < LogLevel.WARN < LogLevel.ERROR < LogLevel.OFF
 *
 * @param level - The Log Level at or above it should log
 */
export function log(level?: number) {
  if (level === undefined) level = LogLevel.WARN
  return MethodDecoratorFactory.createDecorator<LevelMetadata>(LogMetadataKey, {
    level
  })
}

/**
 * Fetch log level stored by `@log` decorator.
 *
 * @param controllerClass - Target controller
 * @param methodName - Target method
 */
export function getLogMetadata(controllerClass: Constructor<{}>, methodName: string): LevelMetadata {
  return (
    MetadataInspector.getMethodMetadata<LevelMetadata>(LogMetadataKey, controllerClass.prototype, methodName) ?? {
      level: LogLevel.OFF
    }
  )
}
