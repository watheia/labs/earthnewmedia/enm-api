// Copyright IBM Corp. 2018,2019. All Rights Reserved.
// Node module: @loopback/example-log-extension
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { Constructor, Getter, inject, Provider } from "@loopback/context"
import { CoreBindings } from "@loopback/core"
import { OperationArgs, Request } from "@loopback/rest"
import chalk from "chalk"
import { getLogMetadata } from "../decorators"
import { LogBindings, LogLevel } from "../keys"
import { HighResTime, LevelMetadata, LogFn, LogWriterFn, TimerFn } from "../types"

export class LogActionProvider implements Provider<LogFn> {
  // LogWriteFn is an optional dependency and it falls back to `logToConsole`
  @inject(LogBindings.LOGGER, { optional: true })
  writeLog: LogWriterFn = logToConsole

  @inject(LogBindings.APP_LOG_LEVEL, { optional: true })
  logLevel: LogLevel = LogLevel.WARN

  constructor(
    @inject.getter(CoreBindings.CONTROLLER_CLASS)
    private readonly getController: Getter<Constructor<{}>>,
    @inject.getter(CoreBindings.CONTROLLER_METHOD_NAME)
    private readonly getMethod: Getter<string>,
    @inject(LogBindings.TIMER) public timer: TimerFn
  ) {}

  value(): LogFn {
    const fn = <LogFn>((
      req: Request,
      args: OperationArgs,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      result: any,
      start?: HighResTime
    ) => {
      return this.action(req, args, result, start)
    })

    fn.startTimer = () => {
      return this.timer()
    }

    return fn
  }

  private async action(
    req: Request,
    args: OperationArgs,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    result: any,
    start?: HighResTime
  ): Promise<void> {
    console.log("log.action(req, args, result, start)", req, args, result, start)
    const controllerClass = await this.getController()
    const methodName: string = await this.getMethod()

    const metadata: LevelMetadata = getLogMetadata(controllerClass, methodName)
    const level: number | undefined = metadata ? metadata.level : undefined

    console.log("controllerClass = ", controllerClass)
    console.log("methodName = ", methodName)
    console.log("level = ", level)
    console.log("this.logLevel = ", this.logLevel)

    if (level !== undefined && this.logLevel !== LogLevel.OFF && level >= this.logLevel && level !== LogLevel.OFF) {
      if (!args) args = []
      let msg = `${req.url} :: ${controllerClass.name}.`
      msg += `${methodName}(${args.join(", ")}) => `

      if (typeof result === "object") msg += JSON.stringify(result)
      else msg += result

      if (start) {
        const timeDiff: HighResTime = this.timer(start)
        const time: number = timeDiff[0] * 1000 + Math.round(timeDiff[1] * 1e-4) / 100
        msg = `${time}ms: ${msg}`
      }

      this.writeLog(msg, level)
    }
  }
}

function logToConsole(msg: string, level: number) {
  let output
  switch (level) {
    case LogLevel.DEBUG:
      output = chalk.white(`DEBUG: ${msg}`)
      break
    case LogLevel.INFO:
      output = chalk.green(`INFO: ${msg}`)
      break
    case LogLevel.WARN:
      output = chalk.yellow(`WARN: ${msg}`)
      break
    case LogLevel.ERROR:
      output = chalk.red(`ERROR: ${msg}`)
      break
  }
  if (output) console.log(output)
}
