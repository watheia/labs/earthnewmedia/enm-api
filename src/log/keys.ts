// Copyright IBM Corp. 2018. All Rights Reserved.
// Node module: @loopback/example-log-extension
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { BindingKey } from "@loopback/context"
import { TimerFn, LogFn, LogWriterFn } from "./types"

/**
 * Binding keys used by this component.
 */
export namespace LogBindings {
  export const APP_LOG_LEVEL = BindingKey.create<LogLevel>("enm.site.api.log.level")
  export const TIMER = BindingKey.create<TimerFn>("enm.site.api.log.timer")
  export const LOGGER = BindingKey.create<LogWriterFn>("enm.site.api.log.logger")
  export const LOG_ACTION = BindingKey.create<LogFn>("enm.site.api.log.action")
}

/**
 * The key used to store log-related via @loopback/metadata and reflection.
 */
export const LogMetadataKey = "enm.site.api.log.metadata"

/**
 * Enum to define the supported log levels
 */
export enum LogLevel {
  DEBUG,
  INFO,
  WARN,
  ERROR,
  OFF
}
